"use strict";

// ===================================
// Libraries
// ===================================
const chat 	= require("discord.io"),
	path 	= require("path"),
	Fb 		= require("firebase"),
	cron 	= require("node-cron"),
	fs 		= require("fs");

// ===================================
// Modules
// ===================================
let TOKEN 		= require("./core/tokens"),
	command 	= require("./core/command"),
	logger 		= require("./core/logger"),
	persona 	= require("./core/personality"),
	userdata 	= require("./core/userdata"),
	status 		= require("./core/presence"),
	react 		= require("./core/react"),
	helper		= require("./core/helpers"),
	vars 		= require("./core/vars"),
	dio 		= require("./core/dio"),
	//	wiki		= require("./core/wiki"),
	messageManager = require("./core/messages"),
	lucilleCmd  = require("./cmds/lucille");

// ===================================
// Initialize Firebase Stuff
// ===================================
let fire = false;
if ( TOKEN.FBKEY2() != false ) {
	Fb.initializeApp(vars.firebasecfg);

	fire = {
		soldiers: 	Fb.database().ref("players"),
		quotes: 	Fb.database().ref("quote"),
		balance	: 	Fb.database().ref("balance"),
		activity: 	Fb.database().ref("activity"),
		bugs: 		Fb.database().ref("bugs")
	};

	// Ready the user data
	userdata = new userdata(fire.soldiers);
	userdata.load(); // ! -- Might be unneeded?

	logger.log("Public Firebase initialized!", logger.MESSAGE_TYPE.OK);
} else {
	logger.log("Public Firebase not initialized.", logger.MESSAGE_TYPE.Warn);
}

//Update the unit stats on the TnT wiki
//if ( helper.isHeroku() ) wiki.mwEditUnitData();

// ====================
// Bot Personas
// ====================
let mjPersona = new persona("Pocketbot", "./assets/avatars/mj.png", vars.emojis.ryionbot),
	mastabot = new persona("Pocketbot", "./assets/avatars/mastabot.png", vars.emojis.mastabot),
	bookbot = new persona("Pocketbot", "./assets/avatars/bookbot.png", vars.emojis.bookbot),
	bookbotCowboy = new persona("Last Man Standing", "./assets/avatars/lms.png", vars.emojis.bookbot),
	lucille = new persona("Pocketbot", "./assets/avatars/lucille.png", vars.emojis.lucille);

// Manager and Groups
let globalCmdManager	= new command.CommandManager("d"),
	basicCmdGroup 		= new command.CommandGroup("basic", mastabot, "Basic commands"),
	ryionbotCmdGroup 	= new command.CommandGroup("ryionbot", mjPersona, "Commands brought to you by RyionBot"),
	ryionbot_ecoCmdGroup= new command.CommandGroup("economy", mjPersona, "RyionBot's currency system commands"),
	matchCmdGroup 		= new command.CommandGroup("matchmake", mastabot, "Handles all the matchmaking commands"),
	quoteCmdGroup 		= new command.CommandGroup("quote", mastabot, "Let's you interface with the quoting system"),
	communityCmdGroup 	= new command.CommandGroup("community", mastabot, "All the basic, most-used community chat commands"),
	keyCmdGroup 		= new command.CommandGroup("key", mastabot, "Alpha tester onboarding commands"),
	adminCmdGroup 		= new command.CommandGroup("admin", mastabot, "Admin/Mod only commands"),
	bookbotCmdGroup		= new command.CommandGroup("bookbot", bookbot, "Informational commands"),
	tourneyCmdGroup		= new command.CommandGroup("tourney", mastabot, "Pocketcup Tournament commands"),
	lmsCmdGroup			= new command.CommandGroup("lms", bookbotCowboy, "Commands for Last Man Standing, a chat minigame made by Glyde"),
	infoCmdGroup		= new command.CommandGroup("unitinfo", mastabot, "Unit/Trait Information command"),
	streamCmdGroup		= new command.CommandGroup("streaming", mastabot, "Twitch stream related commands"),
	lucilleCmdGroup 	= new command.CommandGroup("lucille", lucille, "Lucille gives us the latest tweets."),
	emojiCmd			= new command.CommandGroup("emoji", mastabot, "All the jumbo emotes");

globalCmdManager.addGroup(basicCmdGroup);
globalCmdManager.addGroup(matchCmdGroup);
globalCmdManager.addGroup(quoteCmdGroup);
globalCmdManager.addGroup(communityCmdGroup);
globalCmdManager.addGroup(keyCmdGroup);
globalCmdManager.addGroup(adminCmdGroup);
globalCmdManager.addGroup(ryionbotCmdGroup);
globalCmdManager.addGroup(ryionbot_ecoCmdGroup);
globalCmdManager.addGroup(bookbotCmdGroup);
globalCmdManager.addGroup(lmsCmdGroup);
globalCmdManager.addGroup(infoCmdGroup);
globalCmdManager.addGroup(streamCmdGroup);
globalCmdManager.addGroup(lucilleCmdGroup);
globalCmdManager.addGroup(emojiCmd);
globalCmdManager.addGroup(tourneyCmdGroup);

// Clear the log file
logger.clearLogFile();

// Parse the cmds dir and load any commands in there
fs.readdir(path.join(__dirname, "cmds"), function(err, files){
	if(err){
		logger.log(err, logger.MESSAGE_TYPE.Error);
		return;
	}

	for(let i = 0; i < files.length; i++){
		let _cmds = require(path.join(__dirname, "cmds", path.parse(files[i]).name)).commands;

		_cmds.forEach(function(element) {
			globalCmdManager.addCommand(element);
		}, this);
	}
});

logger.log(`Debug mode: ${helper.isDebug()}`, logger.MESSAGE_TYPE.Info);
logger.log(`Running on Heroku: ${helper.isHeroku() ? "true" : "false"}`, logger.MESSAGE_TYPE.Info);

// This stays a var. You change it back to let, we fight
var bot = new chat.Client({ token: TOKEN.TOKEN, autorun: true });

//Init the messageManager
var globalMessageManager = new messageManager.MessageManager();
globalMessageManager.AddChannels(bot);

// ===================================
// Bot Events
// ===================================

bot.on("ready", function() {
	logger.log("Bot logged in successfully.", logger.MESSAGE_TYPE.OK);
	//helper.popCommand( cList );

	// Work around to giving Lucille bot/persona info!
	if (helper.isHeroku() ) lucilleCmd.sendData({
		// User data created by bots
		userdata: userdata,
		// Command manager
		commandManager: globalCmdManager,
		// Message manager
		messageManager: globalMessageManager,
		// Bot client object
		bot: bot,
		// ID of channel the message was sent in
		channelID: vars.chan,
		// ID of the server(guild)
		serverID: vars.chan
	},lucille);
});

bot.on("disconnect", function(err, errcode) {
	logger.log(`Disconnected from Discord... Message: "${err}" Error Code: "${errcode}"`, "Error");
	bot.connect();
});

if (!process.argv.includes("--ignore-presence")) {
	bot.on("presence", function(user, userID, state, game, event) {
		let statusData = {
			// Bot client object
			bot: bot,
			// Name of user who sent the message
			user: user,
			// ID of user who sent the message
			userID: userID,
			// Raw message string
			state: state,
			// Name of game being player OR stream title
			game: game,
			// Reference to the Firebase DB's
			db: fire,
			// Raw event
			e: event
		};

		if (fire) status.onChange(statusData, userdata);
	});
}

let cList = [],
	spammer = [],
	active = [],
	personCool = false;

bot.on("message", function(user, userID, channelID, message, event) {
	//console.log(`${user} : ${bot.servers[vars.chan].members[userID].roles}`) // Quick role
	//console.log(`${message}`) // Message check

	// Log speaker for the hour
	if ( !active.includes(userID) ) active.push(userID);

	//Remove whitespace
	message = helper.collapseWhitespace(message);

	//Split message into args
	let args = helper.getArgs(message);

	//Prepare command_data object
	let command_data = {
		// User data created by bots
		userdata: userdata,
		// Command manager
		commandManager: globalCmdManager,
		// Message manager
		messageManager: globalMessageManager,
		// Bot client object
		bot: bot,
		// Name of user who sent the message
		user: user,
		// ID of user who sent the message
		userID: userID,
		// ID of channel the message was sent in
		channelID: channelID,
		// ID of the server(guild)
		serverID: vars.chan,
		// Raw message string
		message: message,
		// ID of the message sent
		messageID: event.d.id,
		// Attachments Array
		attachments: event.d.attachments,
		// Array of arguments/words in the message
		args: args,
		// Reference to the Firebase DB's
		db: fire,
		// Actually give an event..
		event: event
	};

	globalMessageManager.Push(new messageManager.Message(command_data.messageID, command_data.message, command_data.userID, command_data.channelID, Date.now()), command_data.channelID);

	// If from Mastabot, check for timed message otherwise ignore
	if (userID === bot.id) {
		if (message.includes("🕑")) {
			helper.countdownMessage(event.d.id,message,channelID,5,bot);
		} else {
			// Check for balance embed
			let embed = (event.d.hasOwnProperty("embeds") && event.d.embeds[0]) ? event.d.embeds[0] : null,
				balEmbed = (embed && embed.hasOwnProperty("author") && embed.author.name === "Proposed Balance Change") ? embed.footer.text : null,
				tourneyEmbed = embed && embed.hasOwnProperty("author") && embed.author.name.startsWith("🏆");

			if (balEmbed) {
				fire.balance.child("newchanges").child(embed).update({
					msgID: event.d.id,
					chanID: channelID
				}).then((err)=> {
					if (err) {
						logger.log(err, "Error");
						return false;
					}
					// Add vote reactions
					setTimeout(() => {
						bot.addReaction({
							channelID: channelID,
							messageID: event.d.id,
							reaction: "👍"
						}, (err,res) => { logger.log(`${err} | ${res}`, "Error"); });
					}, 300);
					setTimeout(() => {
						bot.addReaction({
							channelID: channelID,
							messageID: event.d.id,
							reaction: "👎"
						}, (err,res) => { logger.log(`${err} | ${res}`, "Error"); });
					}, 600);
				});
			}

			if (tourneyEmbed) {
				// Pin it
				bot.pinMessage({
					channelID: channelID,
					messageID: event.d.id
				});
			}
			
			return false;
		}
	}

	// ===================================
	// SPAM Control
	// ===================================
	if (userID !== bot.id && userID !== vars.mbot) { // Ignore self/matchbot
		let speaker = bot.servers[vars.chan].members[userID];

		// Ignore on debug, comment if you need to test
		if (!helper.isDebug()) {
			if (speaker) {
				// Ignore mods/devs
				if (speaker.hasOwnProperty("roles") && (speaker.roles.includes(vars.mod) || speaker.roles.includes(vars.admin))) {
					// Don't do anything
				} else {
					cList.push(userID);
				}

				let c = helper.getCount(cList,userID); // Check how many messages user has posted recently
				//if (channelID != vars.testing) return false;
				if (c===3) {
					if ( spammer.includes(userID) ) {
						// 2nd warning = automute
						helper.muteID({
							data: command_data,
							muteme: userID
						});
					} else {
						// Record warning, auto delete after 5 min.
						spammer.push(userID);
						setTimeout( function() {
							spammer.splice( spammer.indexOf(userID) , 1);
						},1000*60*5);

						// Warning
						let v = [
								`Take it easy on the spam <@${userID}>. :warning:`,
								`<@${userID}> simmer down please. :neutral_face:`,
								`<@${userID}> take a chill pill. :pill:`,
								`Calm down <@${userID}>, no one likes the spam. :unamused:`
							],
							n = Math.floor( Math.random()*4 );
						dio.say(v[n], command_data);
					}
				} else if (c>4) {
					dio.say(`<@${userID}>, you are going to be muted for the next 2 minutes. Please adjust your chat etiquette.`, command_data);

					// Add them to mute role, and remove in 2 minutes
					helper.muteID({
						data: command_data,
						muteme: userID
					});
				}

				setTimeout( function() {
					cList.splice( cList.indexOf(userID) , 1);
				},3000);
			}
		}
	}

	// ===================================
	// Toxic Language
	// ===================================
	const toxic = require("./assets/bleep.json"),
		tuser = bot.servers[vars.chan].members[userID],
		uRoles = (tuser && tuser.hasOwnProperty("roles")) ? tuser.roles : [];

	if (!uRoles.includes(vars.mod) && !uRoles.includes(vars.admin)) {
		for (let x = 0; x < toxic.words.length; x += 1) {
			if (message.toLowerCase().includes(toxic.words[x])) {
				let embed = new helper.Embed({
					author: {
						name: "Language Detected"
					},
					color: 0xFFDC00,
					description: `:speak_no_evil: <#${channelID}> | <@${userID}> said... \`\`\`${message}\`\`\`\n Should I issue a warning? \n ___`,
					footer: {
						text: "(React to this message with a thumbs up or down to warn or dismiss, respectively."
					}
				});

				embed.fields = [
					{
						name: "Channel ID:",
						value: channelID,
						inline: true
					},
					{
						name: "User ID:",
						value: userID,
						inline: true
					}
				];

				dio.sendEmbed(embed, command_data, vars.modchan);
				break;
			}
		}
	}

	try {
		// Log direct messages to Pocketbot OR all messages in debug mode
		if (channelID === vars.modchan) {
			// Stop spying on us Freakspot.
		} else if (!(channelID in bot.directMessages)) {
			if ( helper.isDebug() ) logger.log(`[#${bot.servers[vars.chan].channels[channelID].name}] ${user}: ${message}`);
		} else {
			logger.log(`[DIRECT MESSAGE] ${user}: ${message}`);
		}

		let cmd_trigger = globalCmdManager.isTrigger(args);

		if (message && cmd_trigger){
			let cmd = globalCmdManager.getCommand(args);
			let cmdGroup = globalCmdManager.getGroup(cmd.groupName);

			command_data.trigger = cmd_trigger;

			// Personality Check
			if (globalCmdManager.activePersona != cmdGroup.personality) {
				if (!personCool) {
					cmdGroup.personality.set(command_data, function() {
						globalCmdManager.call(command_data, cmd, cmdGroup);
					});

					globalCmdManager.activePersona = cmdGroup.personality;

					personCool = true;
					setTimeout( () => {
						personCool = false;
					}, 1000*60*10);
				} else {
					globalCmdManager.call(command_data, cmd, cmdGroup);
				}
			} else {
				globalCmdManager.call(command_data, cmd, cmdGroup);
			}
		} else {
			return;
		}
	} catch(e) {
		bot.sendMessage({
			to: channelID,
			message: `An error occured while looking up or trying to call \`${args[0]}\``
		});

		logger.log(`An error occured while looking up or trying to call \`${args[0]}\``, "Error", e);
	}
});

bot.on("guildMemberAdd", function(member) {
	logger.log(`New User: ${member.username} aka ${member.nick} | ${member.id}`, "Info");
	if ( !helper.isHeroku() ) return false;

	let from = `<@${member.id}>`, //(member.nick) ? member.nick : member.username,
		data = {bot: bot, db: fire};

	// Stuff to tell new person
	let v = [
		`Welcome to the Pocketwatch chat, ${from}.`,
		`Ahoy ${from}, welcome!`,
		`Glad to have you here, ${from}.`,
		`What's up ${from}, welcome to the chat!`
	];
	let n = Math.floor( Math.random()*v.length );
	dio.say(v[n], data, vars.chan);

	dio.say("Glad you found the Pocketwatch community, we hope you enjoy your stay. :)\n\n"+

`If you haven't already, please checkout the <#${vars.rules}> channel for some basic community rules. If you ever need my help, feel free to type in \`!help\` in any channel or here in a private message. :thumbsup:

The Recruit role is given to those who own the game. If the bot detects you playing the game, you should be auto-roled. If not, just let a moderator know. `,data,member.id);
});

bot.on("guildMemberRemove", function(member) {
	let fromID = (member && member.id) ? member.id : false;
	if (!fromID) return false;

	logger.log(`User left server: ${member.username} aka ${member.nick} | ${member.id}`, "Info");
	// Check for data off this ID
	fire.soldiers.child(fromID).once("value").then( (snap)=> {
		// If we get a snapshot from the FB, nuke it by setting blank object
		if ( snap.val() ) fire.soldiers.child(fromID).set({});
	});
});

// React to reactions
bot.on("messageReactionAdd", function(e) {
	react.onReact(bot, fire, globalCmdManager, userdata, e);
});

bot.on("messageReactionRemove", function(e) {
	react.onReact(bot, fire, globalCmdManager, userdata, e, true);
});

// Generalized Activity Tracking (every hour)
cron.schedule("0 0 * * * *", function() {
	if ( !helper.isHeroku() ) return false;
	logger.log("Logging activity for past hour", "OK");
	// Currently LFG
	let playing = Object.values(bot.servers[vars.chan].members).filter( (u)=> {
		return u.roles.includes(vars.lfg);
	});

	// Push it
	fire.activity.push({
		active: active.length,
		playing: playing.length,
		time: Date.now()
	});

	// Reset
	active = [];
});

// Pocketbot Cup Tournaments
let tourneyHrs = [18,23];
cron.schedule(`0 0 ${tourneyHrs[0]},${tourneyHrs[1]} * * 1`, function() {
	logger.log("Creating Cup.", "OK");
	let cmd = globalCmdManager.getCommand(["!makecup"]);
	let cmdGroup = globalCmdManager.getGroup(cmd.groupName),
		cdata = { args: ["!makecup"], trigger: "!makecup", bot: bot , channelID: vars.testing, userID: bot.id, commandManager: globalCmdManager };

	globalCmdManager.call(cdata, cmd, cmdGroup);

	const time = new Date();
	dio.say(`Attention <@&${vars.lfg}>, <@&${(time.getHours() === 23) ? vars.na : vars.eu}>, or those wanting to, a new Pocketbot Cup has just opened signups in <#${vars.pbcup}>!`, {bot: bot, db: fire}, vars.memchan);
});

cron.schedule(`0 45 ${tourneyHrs[0]},${tourneyHrs[1]} * * 1`, function() {
	logger.log("Reminding about Cup.", "OK");
	let cmd = globalCmdManager.getCommand(["!checkinremind"]);
	let cmdGroup = globalCmdManager.getGroup(cmd.groupName),
		cdata = { args: ["!checkinremind"], trigger: "!checkinremind", bot: bot , channelID: vars.testing, userID: bot.id, commandManager: globalCmdManager };

	globalCmdManager.call(cdata, cmd, cmdGroup);
	dio.say(`For anyone <@&${vars.lfg}> or just hanging around, there is a Pocketbot Cup currently open for signups, it starts in 15 minutes over in <#${vars.pbcup}>. Go win some ${vars.emojis.wip}!`, {bot: bot, db: fire}, vars.memchan);
});

cron.schedule(`59 59 ${tourneyHrs[0]},${tourneyHrs[1]} * * 1`, function() {
	logger.log("Starting Cup.", "OK");
	let cmd = globalCmdManager.getCommand(["!startcup"]);
	let cmdGroup = globalCmdManager.getGroup(cmd.groupName),
		cdata = { args: ["!startcup"], trigger: "!startcup", bot: bot , channelID: vars.testing, userID: bot.id, commandManager: globalCmdManager };

	globalCmdManager.call(cdata, cmd, cmdGroup);
});